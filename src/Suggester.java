import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.common.collect.ImmutableList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.spell.DirectSpellChecker;
import org.apache.lucene.search.spell.SuggestMode;
import org.apache.lucene.search.spell.SuggestWord;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Suggester {

    private static Iterable<Document> generateDocuments() {
        Document doc1 = new Document();
        Field field1 = new TextField("text", "The quick brown fox jumps over the lazy dog", Field.Store.YES);
        doc1.add(field1);

        Document doc2 = new Document();
        Field field2 = new TextField("text", "Hello world from Vilnius JUG", Field.Store.YES);
        doc2.add(field2);

        return ImmutableList.of(doc1, doc2);
    }

    public static void main(String[] args) throws IOException, ParseException {

        Version luceneVersion = Version.LUCENE_46;
        Analyzer analyzer = new StandardAnalyzer(luceneVersion);

        Path indexFolder = Paths.get("/", "tmp", "LuceneIndex");        // /tmp/LuceneIndex
        if (!Files.exists(indexFolder)) {
            Files.createDirectory(indexFolder);
        }

        Directory directory = FSDirectory.open(indexFolder.toFile());
        IndexWriterConfig indexWriterConfig =
                new IndexWriterConfig(luceneVersion, analyzer)
                        .setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try (IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig)) {
            indexWriter.addDocuments(generateDocuments(), analyzer);
            indexWriter.commit();

            try (IndexReader reader = DirectoryReader.open(directory)) {

                DirectSpellChecker spellChecker = new DirectSpellChecker();
                Term wrongTerm = new Term("text", "quik");
                SuggestWord[] suggestions = spellChecker.suggestSimilar(wrongTerm, 10, reader, SuggestMode.SUGGEST_ALWAYS);
                for (SuggestWord suggestWord : suggestions) {
                    System.out.println("Did you mean \"" + suggestWord.string + "\"?");
                }
            }
        }
    }
}
