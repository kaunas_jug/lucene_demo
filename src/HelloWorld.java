import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Scanner;

import com.google.common.collect.ImmutableList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class HelloWorld {

    private static Query fromInput(Version version, Analyzer analyzer) throws IOException, ParseException {
        Scanner reader = new Scanner(System.in);
        System.out.print("query: ");
        String query = reader.next();
        return new QueryParser(version, "text", analyzer).parse(query);
    }

    private static Iterable<Document> generateDocuments() {
        Document doc1 = new Document();
        Field field1 = new TextField("text", "The quick brown fox jumps over the lazy dog", Field.Store.YES);
        doc1.add(field1);

        Document doc2 = new Document();
        Field field2 = new TextField("text", "Hello world from Kaunas JUG", Field.Store.YES);
        doc2.add(field2);

        return ImmutableList.of(doc1, doc2);
    }

    private static void printDoc(Document doc) {
        Iterator<IndexableField> iterator = doc.iterator();
        while (iterator.hasNext()) {
            IndexableField field = iterator.next();
            System.out.print("Field: name=[" + field.name() + "] value=[" + field.stringValue() + "] ");
        }
        System.out.println();
    }

    public static void main(String[] args) throws IOException, ParseException {

        Version luceneVersion = Version.LUCENE_46;
        Analyzer analyzer = new StandardAnalyzer(luceneVersion);

        Path indexFolder = Paths.get("/", "tmp", "LuceneIndex");        // /tmp/LuceneIndex
        if (!Files.exists(indexFolder)) {
            Files.createDirectory(indexFolder);
        }

        Directory directory = FSDirectory.open(indexFolder.toFile());
        IndexWriterConfig indexWriterConfig =
                new IndexWriterConfig(luceneVersion, analyzer)
                        .setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try (IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig)) {
            indexWriter.addDocuments(generateDocuments(), analyzer);
            indexWriter.commit();

            try (IndexReader reader = DirectoryReader.open(directory)) {
                IndexSearcher searcher = new IndexSearcher(reader);

                Query query = new QueryParser(luceneVersion, "text", analyzer)
                        .parse("text:for fox sake");            // (pun intended)
//                        .parse("JUX~");
//                Query query = fromInput(luceneVersion, analyzer);
                System.out.println("Parsed query: [" + query + "]");

                TopDocs results = searcher.search(query, 10);
                System.out.println("Total hits: " + results.totalHits);
                for (int i=0; i<results.scoreDocs.length; i++) {
                    System.out.print("hit: score=" + results.scoreDocs[i].score + " ");
                    printDoc(reader.document(results.scoreDocs[i].doc));
                }
            }
        }
    }
}
